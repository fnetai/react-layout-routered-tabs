import React from 'react';
import Paper from '@mui/material/Paper';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';

import { HashRouter as Router, Routes, Route, Link, useLocation, useNavigate } from 'react-router-dom';

function RouterTabsInternal({ children }) {
  const location = useLocation();
  const navigate = useNavigate();

  React.useEffect(() => {
    if (location.pathname === '/') {
      const child = children[0];
      if (child) navigate(child.props.path);
    }
  }, [location.pathname]);

  return (
    <React.Fragment>
      <Paper square>
        <Tabs value={location.pathname}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
        >
          {React.Children.map(children, (child) => (
            <Tab
              label={child.props.title || child.props.path.substring(1)}
              value={child.props.path}
              component={Link}
              to={child.props.path}
            />
          ))}
        </Tabs>
      </Paper>
      <Routes>
        {React.Children.map(children, (child) => (
          <Route path={child.props.path} element={child} />
        ))}
      </Routes>
    </React.Fragment>
  );
}

function RouterTab({ children }) {
  return <>{children}</>;
}

function RouterTabs(props) {
  return (
    <Router>
      <RouterTabsInternal {...props} />
    </Router>
  );
}

export { RouterTabs, RouterTab };

export default RouterTabs;