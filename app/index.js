import React from "react";

import RouterTabs, { RouterTab } from "../src";

import Periodic from "./periodic";
import Spline from "./spline";
import Shader from "./shader";

export default () => {
  return (
    <RouterTabs>
      <RouterTab path="/periodic">
        <Periodic />
      </RouterTab>
      <RouterTab path="/spline">
        <Spline />
      </RouterTab>
      <RouterTab path="/shader">
        <Shader />
      </RouterTab>
    </RouterTabs>
  )
}